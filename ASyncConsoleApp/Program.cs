﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ASyncConsoleApp
{
    class Program
    {
        static int Factorial(int n)
        {
            int res = 1;
            for(int i = 2; i <=n; i++)
            {
                res *= i;
            }
            Thread.Sleep(5000);
            //Console.WriteLine("Factorial = " + res);
            return res;
        }

        static async void FactorialAsync(int n)
        {
            int x = await Task.Run(()=>Factorial(n));
            Console.WriteLine(x);
        }

        static void Main(string[] args)
        {
            FactorialAsync(10);
            FactorialAsync(7);
            Console.WriteLine("ASync call!");
            FactorialAsync(5);
            FactorialAsync(3);
            Console.ReadKey();
        }
    }
}
