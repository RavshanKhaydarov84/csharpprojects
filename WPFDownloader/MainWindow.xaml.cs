﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Clipboard = System.Windows.Clipboard;

namespace WPFDownloader
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            tbUrl.Text = "http://";
        }

        private void tbUrl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            tbUrl.Text = Clipboard.GetText();
        }

        private void tbDir_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult dlgres = dialog.ShowDialog();
            if (dlgres == System.Windows.Forms.DialogResult.OK)
            {
                tbDir.Text = dialog.SelectedPath.ToString();
            }
        }
        HttpClient client = new HttpClient();

        private async void AsyncDownload()
        {
            try
            {
                using (var stream = await client.GetStreamAsync(tbUrl.Text.ToString()))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        /*
                        String line;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((line = await reader.ReadLineAsync()) != null)
                        {
                            if (pbMain.Value == 100) pbMain.Value = 0;
                            pbMain.Value += 0.01;
                            stringBuilder.Append(line);
                        }*/
                        var res = await client.GetAsync(tbUrl.Text.ToString(), HttpCompletionOption.ResponseHeadersRead);
                        var contentLength = res.Content.Headers.ContentLength;
                        var download = await res.Content.ReadAsStreamAsync();

                        if (pbMain.Value == 0) pbMain.Maximum = res.Content.Headers.ContentLength.Value;
                        long readbuffersize = download.Length;
                        pbMain.Value += readbuffersize / contentLength.Value;
                        String[] splittedUrl = tbUrl.Text.ToString().Split('/');
                        StreamWriter writer = new StreamWriter(tbDir.Text.ToString() + '/' + splittedUrl[splittedUrl.Length - 1].ToString());
                        await download.CopyToAsync(writer.BaseStream);
                        System.Windows.MessageBox.Show("File Downloaded!");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }

        }

        void Download()
        {
            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted   += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
            string[] splittedURL = tbUrl.Text.Split('/');
            client.DownloadFileAsync(new Uri(tbUrl.Text), tbDir.Text + "\\" + splittedURL[splittedURL.Length-1]);
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            pbMain.Maximum = (int)e.TotalBytesToReceive / 100;
            pbMain.Value = (int)e.BytesReceived / 100;
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            System.Windows.MessageBox.Show("Download completed!");
            pbMain.Value = 0;
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            //AsyncDownload();
            Download();
        }
    }
}
