﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace CatchMeButton
{
    public partial class MainForm : Form
    {
        Random rnd = new Random();
        public MainForm()
        {
            InitializeComponent();
            btnCatchMe.TabStop = false;
            btnCatchMe.Location = btnLocationRandom();
        }
        private void InitiateBackgroundColorChange(Object sender)
        {
            foreach (Button b in (sender as MainForm).Controls)
            {
                b.BackColor = Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
                b.Font = new Font(FontFamily.GenericSansSerif, rnd.Next(1, 64));
                b.Size = new Size(10, 10);
                b.AutoSize = true;
            }
            (sender as MainForm).BackColor = Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
            return;
        }
        int x = 0;
        int y = 0;
        private Point btnLocationRandom()
        {
            x = rnd.Next(0, this.ClientSize.Width - btnCatchMe.Width);
            y = rnd.Next(0, this.ClientSize.Height - btnCatchMe.Height);
            return new Point(x, y);
        }
        private void btnCatchMe_MouseEnter(object sender, EventArgs e)
        {
            (sender as Button).Location = btnLocationRandom();
            InitiateBackgroundColorChange(this);
        }

        private void btnCatchMe_Click(object sender, EventArgs e)
        {
            int c = this.Controls.Count;
            MessageBox.Show("You're catched me?!", "Oops!");
            Button btn = new Button();
            btn.Text = "Catch Me!";
            btn.Location = btnLocationRandom();
            btn.Click += btnCatchMe_Click;
            btn.MouseEnter += btnCatchMe_MouseEnter;
            this.Controls.Add(btn);
            btn.TabStop = false;
        }
    }
}
