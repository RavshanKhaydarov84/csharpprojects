﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFCalc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string znak = "";
        string oldnum = "";
        string point = "";
        public MainWindow()
        {
            InitializeComponent();
            point = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
            btnpoint.Content = point;
        }

        private void DigitButtons_Click(object sender, RoutedEventArgs e)
        {
            if (tbRes.Text.Equals("0")) tbRes.Text = "";
            tbRes.Text += (sender as Button).Content.ToString();
        }

        private void btnce_Click(object sender, RoutedEventArgs e)
        {
            tbRes.Text = "0";
            znak = "";
            oldnum = "";
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Escape)
            {
                tbRes.Text = "0";
            }
        }
        /*
         * 
         * */
        private void FuncButtons_Click(object sender, RoutedEventArgs e)
        {
            switch ((sender as Button).Content.ToString())
            {
                case "+":
                    znak = "+";
                    break;
                case "-":
                    znak = "-";
                    break;
                case "*":
                    znak = "*";
                    break;
                case "/":
                    znak = "/";
                    break;
            }
            oldnum = tbRes.Text.ToString();
            tbRes.Text = "0";
        }

        private void btnpoint_Click(object sender, RoutedEventArgs e)
        {
            if(tbRes.Text.ToString().IndexOf(point) < 0)
            {
                tbRes.Text += point;
            }
        }

        private void btnEqual_Click(object sender, RoutedEventArgs e)
        {
            if(znak != "" && oldnum != "")
            {
                string newNum = tbRes.Text.ToString();
                switch (znak)
                {
                    case "+":
                        tbRes.Text = "" + (Convert.ToDouble(oldnum) + Convert.ToDouble(newNum));
                        break;
                    case "-":
                        tbRes.Text = "" + (Convert.ToDouble(oldnum) - Convert.ToDouble(newNum));
                        break;
                    case "*":
                        tbRes.Text = "" + (Convert.ToDouble(oldnum) * Convert.ToDouble(newNum));
                        break;
                    case "/":
                        if (newNum.Equals("0"))
                        {
                            tbRes.Text = "NAN";
                        }
                        else
                        {
                            tbRes.Text = "" + (Convert.ToDouble(oldnum) / Convert.ToDouble(newNum));
                        }                        
                        break;
                }
            }
        }
    }
}
