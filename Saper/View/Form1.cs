﻿using Saper.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saper
{
    public partial class MainFrm : Form
    {
        Field field;
        GroupBox gb;
        public MainFrm()
        {
            InitializeComponent();
            this.Resize += MainFrm_Resize;
            field = new Field(10, 10);
            listBox1.DataSource = field.levels;
            listBox1.DisplayMember = "getName";
            NewGame(10);
        }

        void AddButtons()
        {
            gb = new GroupBox();
            gb.Location = new Point(110, 110);
            gb.Size = new Size(41 * field.N,41 * field.N);
            gb.Parent = this;

            for (int i = 0; i < field.N; i++)
            {
                for (int j = 0; j < field.N; j++)
                {
                    ControlButton btn = new ControlButton(field, i, j)
                    {
                        Width = 40,
                        Height = 40,
                        Location = new Point(j * 41, i * 41),
                        Parent = gb
                    };
                }
            }
            MainFrm_Resize(this, new EventArgs());
        }

        private void MainFrm_Resize(object sender, EventArgs e)
        {
            gb.Location = new Point((this.Width - gb.Size.Width) / 2, 100);
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {

        }

        void NewGame(int n)
        {
            if (n < 5) n = 5;
            if (n > 20) n = 20;
            if(listBox1.SelectedItem != null)
            {
                field = new Field(n, (listBox1.SelectedItem as LevelEl).percent);
            }
            else
            {
                field = new Field(n, 15);
            }
            AddButtons();
            field.Change += Field_Change1;
            field.Lose += Field_Lose;
            field.Win += Field_Win;
        }

        private void Field_Win(object sender, EventArgs e)
        {
            FindCell(Color.Orange);
            MessageBox.Show("You win!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            field.Win -= Field_Win;
        }

        private void Field_Lose(object sender, EventArgs e)
        {
            FindCell(Color.Red);
            MessageBox.Show("You lose!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            field.Change += Field_Change1;
            field.Lose += Field_Lose;
        }

        void FindCell(Color t)
        {
            for(int i = 0; i < field.N; i++)
                for(int j = 0; j < field.N; j++)
                {
                    if(field.cells[i, j].HasMine)
                    {
                        foreach(ControlButton c in gb.Controls)
                        {
                            if(c.i == i && c.j == j)
                            {
                                c.BackColor = t;
                            }
                        }
                    }
                }
        }

        private void Field_Change1(object sender, ChangeArgs e)
        {
            foreach(object b in gb.Controls)
            {
                if((b as ControlButton) != null && (b as ControlButton).i == e.I)
                {
                    if((b as ControlButton).j == e.J)
                    {
                        if(e.MinArr == "0")
                        {
                            (b as ControlButton).Text = "";
                            (b as ControlButton).BackColor = Color.Green;
                        }
                        else
                        {
                            (b as ControlButton).Text = e.MinArr;
                            (b as ControlButton).BackColor = Color.LightGreen;
                        }
                    }
                }
            }
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            gb.Parent = null;
            if(textBox1.Text != "")
            {
                NewGame(Convert.ToInt32(textBox1.Text.ToString()));
            }
            else
            {
                NewGame(10);
            }
        }

    }
}
