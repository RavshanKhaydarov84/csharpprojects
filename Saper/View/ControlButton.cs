﻿using Saper.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saper
{
    class ControlButton : Button
    {
        Field field;
        public int i,j;
        public ControlButton(Field f, int i, int j)
        {
            field = f;
            this.i = i;
            this.j = j;
        }

        public ControlButton(IContainer container)
        {
            container.Add(this);
            //InitializeComponent();
        }
        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            if(mevent.Button == MouseButtons.Right)
            {
                if(!field.cells[i, j].IsOpen)
                {
                    //BackgroundImage = (System.Drawing.Image)// FLAG
                    BackColor = Color.Orange;
                }
            }
            else
            {
                field.OpenCell(i, j);
            }
        }

    }
}
