﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RunningCockroaches
{
    public partial class Form1 : Form
    {
        Button[] buttons;
        Thread[] threads;
        Random random;
        public delegate void dMove(Button btns);
        dMove dmButton;
        int x0 = 0;
        public Form1()
        {
            InitializeComponent();
            buttons = new Button[] { button1, button2, button3, button4, button5 };
            threads = new Thread[buttons.Length];
            x0 = button1.Location.X;
            random = new Random();
            dmButton = new dMove(moveButton);
        }
        public void moveButton(Button btn)
        {
            btn.Location = new Point(btn.Location.X + random.Next(1,5), btn.Location.Y);
            finish(btn);
        }
        public void finish(Button btn)
        {
            if(btn.Location.X + btn.Width >= lFinish.Location.X)
            {
                foreach (Thread t in threads)
                {
                    t.Abort();
                }
                MessageBox.Show(btn.Name + " won!", this.Text);
                foreach (Button b in buttons)
                {
                    b.Location = new Point(15, b.Location.Y);
                }
                btnStart.Enabled = true;
            }
        }
        public void move(object obj)
        {
            while (true)
            {
                this.Invoke(dmButton, obj as Button);
                Thread.Sleep(random.Next(0,10));
            }
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            ParameterizedThreadStart pts = new ParameterizedThreadStart(move);
            for(int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(pts);
                threads[i].Start(buttons[i]);
            }
        }
    }
}
