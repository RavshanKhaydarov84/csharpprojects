﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SvetoforWF
{
    public partial class Form1 : Form
    {
        int iCount = 0;
        public Form1()
        {
            InitializeComponent();
            tmrAuto.Start();
        }

        private void btnSwitch_Click(object sender, EventArgs e)
        {
            switchColor();
        }

        void switchColor()
        {
            if (iCount == 0)
            {
                tbLight.BackColor = Color.Red;
                tmrAuto.Interval = 5000;
            }
            else if (iCount == 1)
            {
                tbLight.BackColor = Color.Yellow;
                tmrAuto.Interval = 3000;
            }
            else
            {
                tbLight.BackColor = Color.Green;
                iCount = -1;
                tmrAuto.Interval = 5000;
            }
            iCount++;
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            btnSwitch.Enabled = !(btnSwitch.Enabled);
            tmrAuto.Enabled = !(tmrAuto.Enabled);
            if (btnSwitch.Enabled)
            {
                btnAuto.Text = "&Manual";
                tmrAuto.Stop();
            }
            else
            {
                btnAuto.Text = "A&uto";
                tmrAuto.Start();
            }
        }

        private void tmrAuto_Tick(object sender, EventArgs e)
        {
            switchColor();
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            tbLight.Text = DateTime.Now.ToString("d");
        }
    }
}
