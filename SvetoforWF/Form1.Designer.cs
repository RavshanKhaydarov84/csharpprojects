﻿namespace SvetoforWF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSwitch = new System.Windows.Forms.Button();
            this.btnAuto = new System.Windows.Forms.Button();
            this.tbLight = new System.Windows.Forms.TextBox();
            this.tmrAuto = new System.Windows.Forms.Timer(this.components);
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnSwitch
            // 
            this.btnSwitch.Font = new System.Drawing.Font("Georgia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSwitch.Location = new System.Drawing.Point(163, 346);
            this.btnSwitch.Name = "btnSwitch";
            this.btnSwitch.Size = new System.Drawing.Size(100, 34);
            this.btnSwitch.TabIndex = 0;
            this.btnSwitch.Text = "&Switch";
            this.btnSwitch.UseVisualStyleBackColor = true;
            this.btnSwitch.Click += new System.EventHandler(this.btnSwitch_Click);
            // 
            // btnAuto
            // 
            this.btnAuto.Font = new System.Drawing.Font("Georgia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAuto.Location = new System.Drawing.Point(163, 387);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(100, 34);
            this.btnAuto.TabIndex = 0;
            this.btnAuto.Text = "&Auto";
            this.btnAuto.UseVisualStyleBackColor = true;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // tbLight
            // 
            this.tbLight.BackColor = System.Drawing.Color.Red;
            this.tbLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbLight.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.tbLight.Location = new System.Drawing.Point(12, 12);
            this.tbLight.Margin = new System.Windows.Forms.Padding(0);
            this.tbLight.Multiline = true;
            this.tbLight.Name = "tbLight";
            this.tbLight.Size = new System.Drawing.Size(402, 328);
            this.tbLight.TabIndex = 1;
            this.tbLight.Text = "00:00:00";
            this.tbLight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tmrAuto
            // 
            this.tmrAuto.Interval = 5000;
            this.tmrAuto.Tick += new System.EventHandler(this.tmrAuto_Tick);
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            this.tmrTime.Interval = 1000;
            this.tmrTime.Tick += new System.EventHandler(this.tmrTime_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(426, 430);
            this.Controls.Add(this.tbLight);
            this.Controls.Add(this.btnAuto);
            this.Controls.Add(this.btnSwitch);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(442, 469);
            this.MinimumSize = new System.Drawing.Size(442, 469);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Светофор [Windows Forms]";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSwitch;
        private System.Windows.Forms.Button btnAuto;
        private System.Windows.Forms.TextBox tbLight;
        private System.Windows.Forms.Timer tmrAuto;
        private System.Windows.Forms.Timer tmrTime;
    }
}

