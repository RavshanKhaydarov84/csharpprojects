﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCar
{
    enum enum_Wheel_Type
    {
        wtFrontLeft  = 0,
        wtFrontRight = 1,
        wtRearLeft   = 2,
        wtRearRight  = 3
    }
    enum enum_Gearbox_Type
    {
        gbtNone = 0,
        gbtManual = 1,
        gbtAutomatic = 2,
        gbtVariator = 3,
        gbtRobotizedMechanics = 4
    }
    class CSize
    {
        public int Width  = 0;
        public int Height = 0;
    }

    class CarComponent
    {
        public CSize Size;
        public float Weight;
        public String WeightStd = "Kg";
        Color color = Color.White;
        public String Serial { get; set; }
        float price = 0;
        public float Price
        {
            get { return price; }
            set { if (value > 0) price = value; }
        }
        public String Currency { get; set; }
        public Color Color { get; set; }

        public CarComponent()
        {
            Size = new CSize();
        }
    }
    class CarEngine: CarComponent
    {
        private bool IsWorking { get; set; }
        public float Volume { get; set; }
        int valves = 0;
        public int Valves
        {
            get { return valves; }
            set { if (value > 0) valves = value; }
        }
        int cylinders = 0;
        public int Cylinders
        {
            get { return cylinders; }
            set { if (value > 0) cylinders = value; }

        }
        int horsepower = 0;
        public int HorsePower
        {
            get { return horsepower; }
            set { if (value > 0) horsepower = value; }
        }
        int torque = 0;
        public int Torque
        {
            get { return torque;}
            set { if (value > 0) torque = value; }
        }
        protected bool Start()
        {
            if (!IsWorking)
            {
                IsWorking = true;
            }
            return true;
        }
        protected bool Stop()
        {
            if (IsWorking)
            {
                IsWorking = false;
            }
            return true;
        }
    }
    class Gearbox : CarComponent
    {
        public enum_Gearbox_Type Type  { get; set; }
        private int level = 0;
        public int Level
        {
            get { return level; }
            set { if (level > 0 && level < 10) level = value; }
        }
        /*****************************
         * Min 1, Max 9
         * */
        public Gearbox(int maxlevel)
        {
            level = maxlevel;
        }
        int curlevel = 0;
        public int CurLevel
        {
            get { return curlevel; }
            set { if(value >= 0 ) curlevel = value; }
        }
    }
    class Rudder: CarComponent
    {
        float maxturnradius = 270;
        public float MaxTurnRadius { get; set; }
    }
    class Body: CarComponent
    {
        /***********************
         * Body Type: Sedan, Coupe, Universal, Liftback, Crossover and so on
         * */
       public String Type { get; set; }
    }
    class Tire : CarComponent
    {
        public String Name = "";
        public int ProfileHeight { get; set; }
        public int ProfileWidth { get; set; }
    }
    class CarWheel: CarComponent
    {
        enum_Wheel_Type Type;
        public int Diameter { get; set; }
        Tire Tire;
        public CarWheel(enum_Wheel_Type wheeltype)
        {
            Tire = new Tire();
            Type = wheeltype;
        }
    }    
    class Car : CarEngine
    {
        public String Name { get; set; }
        public String ModelName { get; set; }
        int produceddate = 2020;
        public int ProducedDate
        {
            get { return produceddate; }
            set { if (value > 2020) produceddate = value; }
        }
        protected Rudder Rudder;
        protected Body Body;
        protected CarWheel [] Wheels;
        protected Gearbox Gearbox;
        public Car()
        {
            Rudder = new Rudder();
            Body = new Body();
            Wheels.Append(new CarWheel(enum_Wheel_Type.wtFrontLeft));
            Wheels.Append(new CarWheel(enum_Wheel_Type.wtFrontRight));
            Wheels.Append(new CarWheel(enum_Wheel_Type.wtRearLeft));
            Wheels.Append(new CarWheel(enum_Wheel_Type.wtRearRight));
            Gearbox = new Gearbox(6);
        }
    }
}
