﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
    class Observer
    {
        public interface IObserver
        {
            void Update(Object obj);
        }
        public interface IObserverable
        {
            void RegisterObserver(IObserver o);
            void RemoveObserver(IObserver o);
            void NotifyObserver();
        }
        public class MessageInfo
        {
            public string Message { get; set; }
        }

        public class Youtube : IObserverable
        {
            MessageInfo message;
            List<IObserver> subscribers;
            public Youtube()
            {
                message = new MessageInfo();
                subscribers = new List<IObserver>();
            }
            public void NotifyObserver()
            {
                foreach (IObserver o in subscribers)
                {
                    o.Update(message);
                }
            }

            public void RegisterObserver(IObserver o)
            {
                subscribers.Add(o);
            }

            public void RemoveObserver(IObserver o)
            {
                subscribers.Remove(o);
            }
        }
            public class Subscriber : IObserver
            {
                public string Name { get; set; }
                IObserverable server;
                public Subscriber(String name, IObserverable o)
                {
                    this.Name = name;
                    this.server = o;
                    this.server.RegisterObserver(this);
                    Console.WriteLine("Subscriber "+this.Name+" registered!");
                }
                public void Update(object obj)
                {
                    MessageInfo message = (MessageInfo)obj;
                    Console.WriteLine(message.Message);
                    Console.WriteLine("Subscriber " + this.Name + " updated!");
            }

            public void Stop()
                {
                    this.server.RemoveObserver(this);
                    this.server = null;
                    Console.WriteLine("Subscriber " + this.Name + " stopped!");
            }
        }
        
    }
}
