﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
    class Singletone
    {
        private static Singletone instance;
        public static Singletone GetSingletone()
        {
            if (instance == null)
            {
                instance = new Singletone();
            }
            return instance;
        }
        private Singletone() { }
        public void info()
        {
            Console.WriteLine("{0}", instance.ToString());
        }
    }
}
