﻿using System;
using System.Collections.Generic;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Singletone s1 = Singletone.GetSingletone();
            Singletone s2 = Singletone.GetSingletone();
            Singletone s3 = Singletone.GetSingletone();
            s1.info();
            Console.WriteLine(s1.GetHashCode());
            Console.WriteLine(s2.GetHashCode());
            Console.WriteLine(s3.GetHashCode());
            Console.WriteLine(s1.GetType().ToString());
            FactoryMethod.Developer dev  = new FactoryMethod.CocaColaCo();
            FactoryMethod.Developer dev2 = new FactoryMethod.PepsiCo();
            dev.Create();
            dev2.Create();
            Adapter.GlancePaper p1 = new Adapter.GlancePaper();
            Adapter.SimplePaper p2 = new Adapter.SimplePaper();
            Adapter.GlancePaper p3 = new Adapter.GlancePaper();
            Adapter.SimplePaper p4 = new Adapter.SimplePaper();
            Adapter.GlancePaper p5 = new Adapter.GlancePaper();
            Adapter.SimplePaper p6 = new Adapter.SimplePaper();
            List<Adapter.Paper> papers = new List<Adapter.Paper>();
            papers.Add(p1);
            papers.Add(p2);
            papers.Add(p3);
            papers.Add(p4);
            papers.Add(p5);
            papers.Add(p6);
            Adapter.PrinterAdapter.printAll(papers);

            Console.Clear();
            Observer.Youtube youtube = new Observer.Youtube();
            Observer.Subscriber subscriber = new Observer.Subscriber("Anvar", youtube);
            Observer.Subscriber subscriber2 = new Observer.Subscriber("Ilya", youtube);
            Observer.Subscriber subscriber3 = new Observer.Subscriber("Rustam", youtube);
            youtube.NotifyObserver();
            subscriber2.Stop();
        }
    }
}
