﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
    public class Adapter
    {
        public class Printer
        {
            public static void print(String s)
            {
                Console.WriteLine(s);
            }
        }
        public interface Paper
        {
            public void PaperPrint(String s);
        }
        public class SimplePaper : Paper
        {
            public void PaperPrint(string s)
            {
                Printer.print("Simple " + s);
            }
        }
        public class GlancePaper : Paper
        {
            public void PaperPrint(string s)
            {
                Printer.print("Glance " + s);
            }
        }
        public class PrinterAdapter
        {
            public static void printAll(List<Paper> papers)
            {
                foreach(Paper p in papers)
                {
                    p.PaperPrint(p.GetType().ToString());
                }
            }
        }
    }
}
