﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
    class FactoryMethod
    {
        public interface Developer
        {
            public string Name { get; set; }
            public Drinks Create();
        }

        public class CocaColaCo : Developer
        {
            public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public Drinks Create()
            {
                return new CocaCola();
            }
        }

        public class PepsiCo : Developer
        {
            public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public Drinks Create()
            {
                return new Pepsi();
            }
        }

        public interface Drinks { }

        public class CocaCola : Drinks
        {
            public CocaCola()
            {
                Console.WriteLine("CocaCola!");
            }
        }

        public class Pepsi : Drinks
        {
            public Pepsi()
            {
                Console.WriteLine("Pepsi!");
            }
        }
    }
}
